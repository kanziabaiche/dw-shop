<?php

function getProducts() {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM products");
  $statement->execute();

  return $statement->fetchAll();
}

function getProductById(int $id) {
  $pdo = $GLOBALS['pdo'];

  $sql = "SELECT * FROM products WHERE product_id=:id;";

  $statement = $pdo->prepare($sql);
  $statement->bindValue(':id', $id);
  $statement->execute();
  return $statement->fetch();
}

function getProductsByQueryParam(array $search) {
  $pdo = $GLOBALS['pdo'];

  $name = $search['q'] ?? null;
  $min_price = $search['min_price'] ?? null;
  $max_price = $search['max_price'] ?? null;

  $sql = "SELECT * FROM products";

  if ($name || ($min_price >= 0 && $max_price)) {
    $sql .= ' WHERE';
  }

  if ($name) {
    $sql .= " name LIKE :name ";
  }

  if ($name && $min_price && $max_price) {
    $sql .= 'AND';
  }

  if ($min_price >= 0 && $max_price) {
    $sql .= " price BETWEEN :min_price AND :max_price";
  }

  try {
    $statement = $pdo->prepare($sql);

    if ($name) {
      $statement->bindValue(':name', '%' . htmlentities($name) . '%');
    }

    if ($min_price >= 0 && $max_price) {
      $statement->bindValue(':min_price', htmlentities($min_price));
      $statement->bindValue(':max_price', htmlentities($max_price));
    }

    $statement->execute();
    return $statement->fetchAll();

  } catch (PDOException $e) {
    echo $e->getMessage();
    die();
  }
}

function getProductByIdWithCategory(int $id) {
  $pdo = $GLOBALS['pdo'];

  $sql = "SELECT * FROM products AS p JOIN category_categorize_product AS c ON p.product_id=c.product_id WHERE p.product_id=:id;";

  $statement = $pdo->prepare($sql);
  $statement->bindValue(':id', $id);
  $statement->execute();
  $products = $statement->fetchAll();

  $prod = [];

  // on boucle pour recuperer les produits un par un
  foreach ($products as $product) {
    // on boucle sur le produit qui est un tableau associatif
    foreach ($product as $key => $value) {
      if ($key !== "category_id") {
        $prod[$key] = $value;
      } else {
        if (!isset($prod['categories'])) {
          // si le tableau categories n'existe pas on le crée
          $prod['categories'] = [];
        }

        /**
         * @var $value correspond à l'id de la category
         */
        array_push($prod['categories'], $value);
        // $prod['categories'][] = $value;
      }
    }

  }

  return $prod;
}

function createOrUpdateProduct($post, string $image_url, int | bool $product_id): array{
  [
    'name' => $name,
    'price' => $price,
    'quantity' => $quantity,
    'weight' => $weight,
    'description' => $description,
  ] = $post;

  $pdo = $GLOBALS['pdo'];

  if ($product_id) {
    $statement = $pdo->prepare("UPDATE products SET name=:name, price=:price, quantity=:quantity, weight=:weight, description=:description, image_url=:image_url WHERE product_id=:product_id");
    $statement->bindValue(':product_id', htmlentities($product_id));
  } else {
    $statement = $pdo->prepare("INSERT INTO products (name, price, quantity, weight, description, image_url) VALUES(:name, :price, :quantity, :weight, :description, :image_url);");
  }

  $statement->bindValue(':name', htmlentities($name));
  $statement->bindValue(':price', htmlentities($price));
  $statement->bindValue(':quantity', htmlentities($quantity));
  $statement->bindValue(':weight', htmlentities($weight));
  $statement->bindValue(':description', htmlentities($description));
  $statement->bindValue(':image_url', htmlentities($image_url));
  $statement->execute();

  // on recupère l'id de la derniere insertion en base de donnée qui correspond ici au produit qu'on vient d'ajouter
  if (!$product_id) {
    $product_id = $pdo->lastInsertId();
  }
  // on retourne le produit qu'on vient d'inserer
  return getProductById($product_id);
}

function updateProductQuantity(int $product_id, int $quantity): void {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("UPDATE products SET quantity=(quantity - :quantity) WHERE product_id=:product_id");
  $statement->bindValue(':quantity', $quantity);
  $statement->bindValue(':product_id', $product_id);
  $statement->execute();
}

function addCategoryToProduct(int $product_id, int $category_id): void {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("INSERT INTO category_categorize_product (product_id, category_id) VALUES(:product_id, :category_id);");
  $statement->bindValue(':product_id', htmlentities($product_id));
  $statement->bindValue(':category_id', htmlentities($category_id));
  $statement->execute();
}

function deleteProduct(int $product_id): bool {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("DELETE FROM products WHERE product_id=:id");
  $statement->bindValue(':id', htmlentities($product_id));
  return $statement->execute();
}

function getMaxPrice() {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT MAX(price) AS product_max_price FROM products;");
  $statement->execute();
  return $statement->fetch();
}