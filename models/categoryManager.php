<?php

function getCategories() {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM categories;");
  $statement->execute();

  return $statement->fetchAll();
}

function getCategoryById(int $category_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM categories WHERE category_id=:id;");
  $statement->bindValue(':id', htmlentities($category_id));
  $statement->execute();

  return $statement->fetch();
}

function createOrUpdateCategory(array $post, int | bool $category_id) {
  $pdo = $GLOBALS['pdo'];

  [
    'name' => $name,
    'color' => $color,
  ] = $post;

  try {
    if (!$category_id) {
      $statement = $pdo->prepare("INSERT INTO categories (name, color) VALUES (:name, :color);");
    } else {
      $statement = $pdo->prepare("UPDATE categories SET name=:name, color=:color WHERE category_id=:id");
      $statement->bindValue(':id', htmlentities($category_id));
    }

    $statement->bindValue(':name', htmlentities($name));
    $statement->bindValue(':color', htmlentities($color));
    $statement->execute();

    return true;
  } catch (PDOException $e) {
    return false;
  }
}

function deleteCategory(int $category_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("DELETE FROM categories WHERE category_id=:id");
  $statement->bindValue(':id', htmlentities($category_id));
  return $statement->execute();
}