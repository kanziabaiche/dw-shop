<?php

function getUsers() {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM users");
  $statement->execute();

  return $statement->fetchAll();
}

function getUserByEmail($email) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM users WHERE email=:email");
  $statement->bindValue(":email", htmlentities($email));
  $statement->execute();

  return $statement->fetch();
}

function getUserById(int $id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM users WHERE email=:id");
  $statement->bindValue(":id", htmlentities($id));
  $statement->execute();

  return $statement->fetch();

}

function getUsersByQueryParam(string $search) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM users WHERE first_name LIKE :search OR last_name LIKE :search OR email LIKE :search");
  $statement->bindValue(':search', '%' . htmlentities($search) . '%');
  $statement->execute();
  return $statement->fetchAll();
}

function createUser(array $post) {
  $pdo = $GLOBALS['pdo'];

  [
    'first_name' => $first_name,
    'last_name' => $last_name,
    'age' => $age,
    'email' => $email,
    'password' => $password,
    'address' => $address,
    'postal_code' => $postal_code,
    'city' => $city,
    'country' => $country,
  ] = $post;

  // on stock la requete dans une variable avec la syntaxe HEREDOC
  $sql = <<< EOT
    INSERT INTO users
    (first_name, last_name, age, email, password, address, postal_code, city, country)
    VALUES (:first_name, :last_name, :age, :email, :password, :address, :postal_code, :city, :country);
  EOT;

  $statement = $pdo->prepare($sql);

  $hashed_password = password_hash($password, PASSWORD_BCRYPT, ['cost' => 14]);
  
  $statement->bindValue(':first_name', htmlentities($first_name));
  $statement->bindValue(':last_name', htmlentities($last_name));
  $statement->bindValue(':age', htmlentities($age));
  $statement->bindValue(':email', htmlentities($email));
  $statement->bindValue(':password', htmlentities($hashed_password));
  $statement->bindValue(':address', htmlentities($address));
  $statement->bindValue(':postal_code', htmlentities($postal_code));
  $statement->bindValue(':city', htmlentities($city));
  $statement->bindValue(':country', htmlentities($country));
  $statement->execute();

  $user_id = $pdo->lastInsertId();
  return getUserById($user_id);
}

function deleteUser(int $user_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("DELETE FROM users WHERE user_id=:id");
  $statement->bindValue(':id', htmlentities($user_id));
  return $statement->execute();
}

function getUserCart(int $user_id) {
  $pdo = $GLOBALS['pdo'];

  $sql = <<< EOT
  SELECT
    c.quantity AS quantity_in_cart,
    p.product_id,
    p.name,
    p.price,
    p.image_url,
    p.weight
  FROM cart AS c
  JOIN products AS p
    ON p.product_id = c.product_id
  WHERE c.user_id = :user_id;
  EOT;

  $statement = $pdo->prepare($sql);
  $statement->bindValue(':user_id', $user_id);
  $statement->execute();
  return $statement->fetchAll();
}