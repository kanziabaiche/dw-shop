<?php

function addProductToCart(array $post) {
  $pdo = $GLOBALS['pdo'];

  [
    'product_id' => $product_id,
    'user_id' => $user_id,
    'quantity' => $quantity,
  ] = $post;

  try {
    $statement = $pdo->prepare("INSERT INTO cart (product_id, user_id, quantity) VALUES(:product_id, :user_id, :quantity);");
    $statement->bindValue(':product_id', htmlentities($product_id));
    $statement->bindValue(':user_id', htmlentities($user_id));
    $statement->bindValue(':quantity', htmlentities($quantity));
    return $statement->execute();
  } catch (PDOException $e) {
    // si une erreur survient on vérifie si dans le message d'erreur on a la chaine "duplicate"
    if (stristr($e->getMessage(), 'duplicate')) {
      updateCartQuantity($product_id, $user_id, $quantity);
    }
  }
}

function updateCartQuantity(int $product_id, int $user_id, int $quantity) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("UPDATE cart SET quantity=(quantity + :quantity) WHERE product_id=:product_id AND user_id=:user_id;");
  $statement->bindValue(':product_id', htmlentities($product_id));
  $statement->bindValue(':user_id', htmlentities($user_id));
  $statement->bindValue(':quantity', htmlentities($quantity));
  $statement->execute();

  $product = getCartItemByProductId($product_id);

  if (!$product['quantity']) {
    deleteCartItemByProductId($product_id);
  }
}

function getCartItemByProductId(int $product_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM cart WHERE product_id=:product_id");
  $statement->bindValue(':product_id', htmlentities($product_id));
  $statement->execute();
  return $statement->fetch();
}

function getCartItemsByUserId(int $user_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("SELECT * FROM cart WHERE user_id=:user_id");
  $statement->bindValue(':user_id', htmlentities($user_id));
  $statement->execute();
  return $statement->fetchAll();

}

function deleteCartItemByProductId(int $product_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("DELETE FROM cart WHERE product_id=:product_id");
  $statement->bindValue(':product_id', htmlentities($product_id));
  return $statement->execute();
}

function deleteCartItemByUserId(int $user_id): bool {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("DELETE FROM cart WHERE user_id=:user_id");
  $statement->bindValue(':user_id', htmlentities($user_id));
  return $statement->execute();
}