<?php

function createOrder(int $user_id) {
  $pdo = $GLOBALS['pdo'];

  $statement = $pdo->prepare("INSERT INTO orders (status, user_id) VALUES('ordered', :user_id)");
  $statement->bindValue(':user_id', htmlentities($user_id));
  $statement->execute();
  $order_id = $pdo->lastInsertId();

  $cart_records = getCartItemsByUserId($user_id);

  foreach ($cart_records as $record) {
    $product_id = $record['product_id'];
    $quantity = $record['quantity'];

    $statement = $pdo->prepare("INSERT INTO order_items (user_id, product_id, order_id, quantity) VALUES (:user_id, :product_id, :order_id, :quantity)");
    $statement->bindValue(':user_id', htmlentities($user_id));
    $statement->bindValue(':product_id', htmlentities($product_id));
    $statement->bindValue(':order_id', htmlentities($order_id));
    $statement->bindValue(':quantity', htmlentities($quantity));
    $statement->execute();

    updateProductQuantity($product_id, $quantity);
    deleteCartItemByUserId($product_id);
  }
}