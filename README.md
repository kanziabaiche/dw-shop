# DW SHOP

- se déplacer dans le dossier du projet dans le terminal
- faire un composer require
- importer la base de donnée
  ```bash
  $ mysql -u root -p < ./database/create_db.sql
  ```
- se connecter à mysql et créer un utilisateur mysql
  ```mysql
  mysql> USE mysql;
  mysql> CREATE USER '<nom_utilisateur>'@'localhost' IDENTIFIED BY '<mot_de_passe>;
  mysql> GRANT ALL PRIVILEGES ON <nom_base_de_donnee> . * TO '<nom_utilisateur>'@'localhost';
  ```
- créer le fichier .env à la racine du projet en se servant du .env.example
- dans le fichier php.ini qui se trouve dans le dossier php de mamp/xampp, enlever le ; devant la ligne extension=pdo_mysql

  - on va avoir quelque chose dans ce genre

  ```bash
  extension=pdo_mysql
  ;extension=pdo_odbc
  ;extension=pdo_pgsql
  ;extension=pdo_sqlite
  ;extension=pgsql
  ;extension=pspell
  ```

- lancer le serveur avec la commande
  ```bash
  $ php -S localhost:8080 -t public
  ```
