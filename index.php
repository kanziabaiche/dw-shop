<?php

session_start();

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/utils/env.php';
require_once __DIR__ . '/utils/forms.php';
require_once __DIR__ . '/utils/index.php';

// on instancie la class FilesystemLoader de twig en lui disant ou trouver les templates qu'on lui demandera d'envoyer
$loader = new \Twig\Loader\FilesystemLoader(__DIR__ . '/views');

// on déclare twig en tant que variable globale
global $twig;
$twig = new \Twig\Environment($loader, [
  'cache' => false,
]);

// on ajoute des variables globales pour nos templates twig
$twig->addGlobal('user', $_SESSION['user'] ?? false);
$uri = $_SERVER['REQUEST_URI'] !== '/' ? $_SERVER['REQUEST_URI'] : 'home';
$twig->addGlobal('active_page', $uri);

require_once __DIR__ . '/database/index.php';

$router = new AltoRouter();

// MANAGERS
require_once __DIR__ . '/models/userManager.php';
require_once __DIR__ . '/models/categoryManager.php';
require_once __DIR__ . '/models/cartManager.php';
require_once __DIR__ . '/models/productManager.php';
require_once __DIR__ . '/models/orderManager.php';

// CONTROLLERS
require_once __DIR__ . '/controllers/mainController.php';
require_once __DIR__ . '/controllers/productsController.php';
require_once __DIR__ . '/controllers/authController.php';
require_once __DIR__ . '/controllers/categoryController.php';
require_once __DIR__ . '/controllers/userController.php';
require_once __DIR__ . '/controllers/orderController.php';
require_once __DIR__ . '/controllers/cartController.php';

require_once __DIR__ . '/routes/index.php';

// on vérifie les correspondance de routes
$match = $router->match();

// on utilise la function redirect() qui permettra de rediriger l'utilisateur en fonctions de conditions particulières
redirect();

// appelle la fonction match['target'] si c'est une fonction ou envoie une 404
if (is_array($match) && is_callable($match['target'])) {
  call_user_func_array($match['target'], $match['params']);
} else {
  // aucune route n'est trouvée
  echo $twig->render('404.twig', ['page_title' => 'Page introuvable']);
}