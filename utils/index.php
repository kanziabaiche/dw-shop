<?php

function redirect() {
  // on cherche dans l'URI le mot admin
  if (stristr($_SERVER['REQUEST_URI'], 'admin')) {
    // si utilisateur en session (connecté) et utilisateur non admin OU pas d'utilisateur en session (connecté)
    if ((isset($_SESSION['user']) && !$_SESSION['user']['is_admin']) || !isset($_SESSION['user'])) {
      header('Location: /');
    }
    // on cherche dans l'URI le mot login
  } elseif (stristr($_SERVER['REQUEST_URI'], 'login') || stristr($_SERVER['REQUEST_URI'], 'inscription')) {
    // si on a un utilisateur en session (connecté)
    if (isset($_SESSION['user'])) {
      header('Location: /');
    }
  }
}

function getUserCartDetails() {
  if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
    // si on a un utilisateur connecté on récupère son panier
    // on appelle la fonction getUserCart() de notre cartManager
    $cart = getUserCart($user['user_id']);
    $cart_total_price = array_reduce($cart,
      fn($accumulator, $current_item) => ($current_item['quantity_in_cart'] * $current_item['price']) + $accumulator);
    $cart_total_items = array_reduce($cart,
      fn($accumulator, $current_item) => $current_item['quantity_in_cart'] + $accumulator);

    return [
      'cart' => $cart ?? null,
      'cart_total_price' => $cart_total_price,
      'cart_total_items' => $cart_total_items,
    ];
  }
}