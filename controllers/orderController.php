<?php

use Stripe\Charge;
use Stripe\Stripe;

function getAdminOrders() {
  $twig = $GLOBALS['twig'];

  echo $twig->render('admin/admin-orders.twig', [
    'page_title' => 'Commandes',
    // 'products' => $products,
    'active_page' => $_SERVER['REQUEST_URI'],
  ]);
}

function getValidateOrderPage() {
  $twig = $GLOBALS['twig'];

  [
    'cart' => $cart,
    'cart_total_price' => $cart_total_price,
    'cart_total_items' => $cart_total_items,
  ] = getUserCartDetails();

  echo $twig->render('order.twig', [
    'page_title' => 'Commande',
    'cart' => $cart ?? null,
    'cart_total_price' => $cart_total_price,
    'cart_total_items' => $cart_total_items,
  ]);

}

function postValidateOrderPage() {
  $twig = $GLOBALS['twig'];

  [
    'is_form_valid' => $is_form_valid,
    'errors' => $errors,
  ] = validateForm($_POST);

  [
    'cart' => $cart,
    'cart_total_price' => $cart_total_price,
    'cart_total_items' => $cart_total_items,
  ] = getUserCartDetails();

  if ($is_form_valid) {
    echo $twig->render('payment.twig', [
      'page_title' => 'Paiement',
      'cart_total_price' => $cart_total_price,
    ]);
    return;
  }

  echo $twig->render('order.twig', [
    'page_title' => 'Commande',
    'cart' => $cart ?? null,
    'cart_total_price' => $cart_total_price,
    'cart_total_items' => $cart_total_items,
    'errors' => $errors,
    'fields' => $_POST,
  ]);
}

function postPayment() {
  $twig = $GLOBALS['twig'];

  Stripe::setApiKey($_ENV['STRIPE_API_KEY']);
  $token = $_POST['stripeToken'] ?? null;
  $amount_to_pay = $_POST['amount_to_pay'] ?? null;
  if ($token && $amount_to_pay) {
    $charge = Charge::create(
      [
        // on multiplie par 100 pour avoir le montant correct
        // stripe comptabilise en centimes
        'amount' => $amount_to_pay * 100,
        'currency' => 'eur',
        'source' => $token,
      ]
    );

    $user_id = $_SESSION['user']['user_id'] ?? null;

    if ($user_id) {
      createOrder($user_id);
    }

    $chargeId = $charge->id;

    if ($chargeId) {
      echo $twig->render('success.twig', ['order_id' => $chargeId]);
    }
  }
}