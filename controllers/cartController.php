<?php

function getCartPage() {
  $twig = $GLOBALS['twig'];

  if (isset($_SESSION['user'])) {
    // si on a un utilisateur connecté on récupère son panier
    // on appelle la fonction getUserCart() de notre cartManager
    $cart = getUserCart($_SESSION['user']['user_id']);
  }

  echo $twig->render('cart.twig', [
    'page_title' => 'Panier',
    'cart' => $cart ?? null,
  ]);
}

function postAddToCart() {
  // pour recuperer du JSON il faut utiliser cette méthode
  $json = file_get_contents("php://input");

  // on décode le json en le transformant en tableau associatif
  $decoded_json = json_decode($json, true);

  ['is_form_valid' => $is_form_valid] = validateForm($decoded_json);

  if ($is_form_valid) {
    addProductToCart($decoded_json);
  }

  // on prépare la réponse à envoyer au frontend
  $response = ['message' => 'article bien ajouté'];
  $json = json_encode($response);

  // on renvoie la réponse encodée en JSON
  echo $json;
}

function postDeleteCartItemByProductId(int $product_id) {
  if (deleteCartItemByProductId($product_id)) {
    echo json_encode(['message' => 'Element supprimé']);
    return;
  }

  echo json_encode(['message' => "Problème durant la suppression de l'élément"]);
}