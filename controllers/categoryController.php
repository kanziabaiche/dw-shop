<?php

function getAdminCategories() {
  $twig = $GLOBALS['twig'];

  $categories = getCategories();

  echo $twig->render('admin/admin-categories.twig', [
    'page_title' => 'Categories',
    'categories' => $categories,
  ]);
}

function getAddAdminCategory() {

  $twig = $GLOBALS['twig'];

  echo $twig->render('admin/add-category.twig', [
    'page_title' => 'Categories',
  ]);
}

function postAddAdminCategory(int $category_id) {
  $twig = $GLOBALS['twig'];

  [
    'is_form_valid' => $is_form_valid,
    'errors' => $errors,
  ] = validateForm($_POST);

  if ($is_form_valid) {
    if (createOrUpdateCategory($_POST, $category_id ?? false)) {
      header('Location: /admin/categories');
      return;
    } else {
      $errors['database_error'] = "Un problème est survenu lors de l'ajout";
    }
  }

  echo $twig->render('admin/add-category.twig', [
    'page_title' => 'Categories',
    'errors' => $errors,
  ]);
}

function deleteAdminCategory(int $category_id) {
  if ($category_id) {
    deleteCategory($category_id);
  }

  header('Location: /admin/categories');
}

function getEditAdminCategory(int $category_id) {
  $twig = $GLOBALS['twig'];

  $category = getCategoryById($category_id);

  echo $twig->render('admin/add-category.twig', [
    'page_title' => 'Categories',
    'category' => $category,
  ]);
}