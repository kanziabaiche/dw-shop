<?php

function getHome() {
  $twig = $GLOBALS['twig'];

  $products = getProducts();

  echo $twig->render('home.twig', ['products' => $products]);
};

function getPaymentPage() {
  $twig = $GLOBALS['twig'];

  echo $twig->render('payment.twig');
}

function getContact() {
  $twig = $GLOBALS['twig'];

  echo $twig->render('contact.twig');
}

function redirectAdmin() {
  header('Location: /admin/produits');
}