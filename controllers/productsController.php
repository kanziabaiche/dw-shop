<?php

function getAdminProducts() {
  $twig = $GLOBALS['twig'];

  if (sizeof($_GET) > 0) {
    $products = getProductsByQueryParam($_GET);
  } else {
    $products = getProducts();
  }

  echo $twig->render('admin/admin-products.twig', [
    'page_title' => 'Produits',
    'products' => $products,
  ]);
}

function getAddAdminProducts() {
  $twig = $GLOBALS['twig'];

  $categories = getCategories();

  echo $twig->render('admin/add-product.twig', [
    'page_title' => 'Ajouter un produit',
    'categories' => $categories,
  ]);
}

function postAddAdminProducts(int | bool $product_id = false) {
  $is_file_uploaded = false;

  ['file' => $file] = $_FILES;

  if ($file['error'] === UPLOAD_ERR_OK) {
    $allowed_file_types = ['jpg', 'jpeg', 'png', 'gif'];

    [
      'name' => $file_name_with_extension,
      'tmp_name' => $file_tmp_name,
    ] = $file;

    [
      'filename' => $file_name,
      'extension' => $file_extension,
    ] = pathinfo($file_name_with_extension);

    if (in_array($file_extension, $allowed_file_types)) {
      $destination_folder = "/public/uploads/products";
      $destination = dirname(__DIR__) . $destination_folder;

      if (!is_dir($destination)) {
        mkdir($destination, 0777, true);
      }

      $new_filename = md5($file_name) . '-' . time() . ".$file_extension";

      // deplacer l'image
      $is_file_uploaded = move_uploaded_file($file_tmp_name, "$destination/$new_filename");
    }

  } elseif ($file['error'] === UPLOAD_ERR_NO_FILE) {
    echo "Pas d'image envoyée !";
  }

  $twig = $GLOBALS['twig'];

  [
    'is_form_valid' => $is_form_valid,
    'errors' => $errors,
  ] = validateForm($_POST);

  if ($is_form_valid && $is_file_uploaded) {
    $image_path = "$destination_folder/$new_filename";

    $product = createOrUpdateProduct($_POST, $image_path, $product_id ?? false);

    if (isset($_POST['categories']) && is_array($_POST['categories'])) {
      foreach ($_POST['categories'] as $category_id) {
        addCategoryToProduct($product['product_id'], $category_id);
      }
    }

    header('Location: /admin/produits');
  }

  $categories = getCategories();

  echo $twig->render('admin/add-product.twig', [
    'page_title' => 'Ajouter un produit',
    'categories' => $categories,
    'errors' => $errors,
    'active_page' => $_SERVER['REQUEST_URI'],
  ]);
}

function deleteAdminProduct(int $product_id) {
  if ($product_id) {
    deleteProduct($product_id);
  }

  header('Location: /admin/produits');
}

function getProductsPage() {
  $twig = $GLOBALS['twig'];

  if (sizeof($_GET) > 0) {
    $products = getProductsByQueryParam($_GET);
  } else {
    $products = getProducts();
  }

  $result = getMaxPrice();
  $product_max_price = $result['product_max_price'] ?? null;

  echo $twig->render('products.twig', [
    'page_title' => 'Produits',
    'products' => $products,
    'product_max_price' => $product_max_price,
    'fields' => $_GET,
  ]);
}

function getProductPage(int $product_id) {
  $twig = $GLOBALS['twig'];

  $product = getProductById($product_id);

  echo $twig->render('product.twig', [
    'page_title' => 'Produits',
    'product' => $product,
  ]);
}

function getEditAdminProducts(int $product_id) {
  $twig = $GLOBALS['twig'];

  $categories = getCategories();
  // $product = getProductByIdWithCategory($product_id);
  $product = getProductById($product_id);

  echo $twig->render('admin/add-product.twig', [
    'page_title' => 'Modifier un produit',
    'categories' => $categories,
    'product' => $product,
  ]);
}

// function postEditAdminProducts(int $product_id) {
//   $twig = $GLOBALS['twig'];

// }