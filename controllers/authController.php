<?php

function getLogin() {
  // je récupère twig depuis mes variables globales
  $twig = $GLOBALS['twig'];

  // ici on envoie la vue login.twig qui se trouve dans le dossier views
  // on pass en 2eme paramètre  de la fonction render un tableau associatif qui permet de passer des informations à la vue
  echo $twig->render('login.twig', [
    'page_title' => "Connexion",
    'button_text' => 'Se connecter',
    'login_page' => true,
  ]);
}

function postLogin() {
  $twig = $GLOBALS['twig'];

  [
    'is_form_valid' => $is_form_valid,
    'errors' => $errors,
  ] = validateForm($_POST);

  // on récupère les données du formulaire
  [
    'email' => $email,
    'password' => $password,
  ] = $_POST;

  if ($is_form_valid) {

    // on fait appelle à la fonction getUserByMail() de notre userManager pour chercher un utilisateur par son email
    //! dans le modele MVC on ne mélange pas les responsabilitées
    $known_user = getUserByEmail($email);

    if ($known_user && password_verify($password, $known_user['password'])) {
      $_SESSION['user'] = $known_user;
      header('Location: /');
      return;
    } else {
      $errors['not_found'] = "email ou mot de passe incorrect";
    }
  }

  echo $twig->render('login.twig', [
    'email' => $email,
    'password' => $password,
    'errors' => $errors,
    'login_page' => true,
    'page_title' => "Connexion",
    'button_text' => 'Se connecter',
  ]);
}

function getSignup() {
  $twig = $GLOBALS['twig'];

  echo $twig->render('signup.twig', [
    'page_title' => "Inscription",
    'button_text' => "S'inscrire",
  ]);
}

function postSignup() {
  $twig = $GLOBALS['twig'];

  [
    'is_form_valid' => $is_form_valid,
    'errors' => $errors,
  ] = validateForm($_POST);

  if ($is_form_valid) {
    if ($_POST['password'] === $_POST['confirm_password']) {
      createUser($_POST);
      header('Location: /login');
      return;
    } else {
      $errors['password_match'] = 'Les mots de passes ne sont pas identiques';
    }
  }

  echo $twig->render('signup.twig', [
    'errors' => $errors,
    'fields' => $_POST,
    'page_title' => "Inscription",
    'button_text' => "S'inscrire",
  ]);
}

function postLogout() {
  unset($_SESSION['user']);
  header("Location: /");
}