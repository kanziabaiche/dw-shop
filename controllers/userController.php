<?php

function getAdminUsers() {

  $twig = $GLOBALS['twig'];

  $search = $_GET['q'] ?? null;

  if ($search) {
    $users = getUsersByQueryParam($search);
  } else {
    $users = getUsers();
  }

  echo $twig->render('admin/admin-users.twig', [
    'page_title' => 'Utilisateurs',
    'users' => $users,
  ]);
}

function deleteAdminUser(int $user_id) {
  if ($user_id) {
    deleteUser($user_id);
  }

  header('Location: /admin/utilisateurs');
}