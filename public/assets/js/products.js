const formElements = Array.from(document.querySelectorAll('form'));

for (const form of formElements) {
  // if (form.classList.contains('search__form')) {
  //   continue;
  // }

  if (form.getAttribute('data-search')) {
    continue;
  }

  form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const button = form.querySelector('button');
    const addToCartText = button.querySelector('span.add-to-cart-text');
    const quantity = +form.querySelector('input[type="number"]').value;
    const product_id = +Array.from(form.querySelectorAll('input[type="hidden"]'))[0].value;
    const user_id = +Array.from(form.querySelectorAll('input[type="hidden"]'))[1].value;

    try {
      await axios.post(
        `https://${
          window.location.hostname === 'localhost'
            ? `${window.location.hostname}:8080`
            : window.location.hostname
        }/api/panier`,
        {
          quantity,
          product_id,
          user_id,
        },
      );

      const addedToCart = form.querySelector('.added-to-cart');

      addToCartText.style.animation = 'slide-to-right 500ms forwards';
      addedToCart.style.animation = 'slide-from-left 500ms 250ms forwards';
      addedToCart.textContent = 'Article ajouté au panier';

      button.style.animation = 'change-color 500ms forwards';
      button.style.pointerEvents = 'none';

      setTimeout(() => {
        button.style.animation = 'change-color-reverse 500ms forwards';
        addToCartText.style.transform = 'translate(-300%,-50%)';
        addToCartText.style.animation = 'slide-from-left 500ms 250ms forwards';
        addedToCart.style.animation = 'slide-to-right 500ms forwards';
        button.style.pointerEvents = 'all';
      }, 2000);
    } catch (error) {
      console.error(error);
    }
  });
}

const lowerSlider = document.querySelector('#lower');
const upperSlider = document.querySelector('#upper');

function updateRangeText(selector) {
  const lowerSlider = document.querySelector('#lower');
  const upperSlider = document.querySelector('#upper');
  const maxVal = +lowerSlider.max;

  const lowerVal = +lowerSlider.value;
  const upperVal = +upperSlider.value;

  const rangeText = document.querySelector(selector);
  rangeText.style.left =
    Math.floor(((selector === '#upper-text' ? upperVal : lowerVal) * 100) / maxVal) + '%';
  rangeText.textContent = (selector === '#upper-text' ? upperVal : lowerVal) + '€';

  if (upperVal < lowerVal) {
    lowerSlider.value = upperVal;
    const lowerText = document.querySelector('#lower-text');
    lowerText.style.left = Math.floor((upperVal * 100) / maxVal) + '%';
    lowerText.textContent = upperVal + '€';
  }
  if (lowerVal > upperVal) {
    upperSlider.value = lowerVal;
    const upperText = document.querySelector('#upper-text');
    upperText.style.left = Math.floor((lowerVal * 100) / maxVal) + '%';
    upperText.textContent = lowerVal + '€';
  }
}

updateRangeText('#upper-text');
updateRangeText('#lower-text');

upperSlider.addEventListener('input', () => updateRangeText('#upper-text'));

lowerSlider.addEventListener('input', () => updateRangeText('#lower-text'));
