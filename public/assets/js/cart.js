function triggerNotification(message) {
  const containerElement = document.createElement('div');
  containerElement.className = 'notification';
  containerElement.textContent = message;

  containerElement.addEventListener('animationend', (event) =>
    setTimeout(() => {
      containerElement.classList.add('notification--disapear');
      setTimeout(() => event.target.remove(), 1000);
    }, 3000),
  );
  document.body.append(containerElement);
}

function updateCartTotal() {
  const quantityInputElements = Array.from(document.querySelectorAll('input[type="number"'));

  let quantity = 0;
  let price = 0;
  for (const input of quantityInputElements) {
    quantity += +input.value;
    price += +input.parentElement.parentElement.nextElementSibling.textContent.split('€')[0];
  }

  const totalQuantityElement = document.querySelector('.total__quantity');
  totalQuantityElement.textContent = quantity;
  const totalPriceElement = document.querySelector('.total__price');
  totalPriceElement.textContent = price + '€';

  return quantityInputElements.length;
}

async function deleteProductFromCart(button, product_id, event) {
  event.preventDefault();

  try {
    await axios.delete(`/api/panier/${product_id}`);

    button.closest('.product').remove();
    triggerNotification('Element supprimé !');
    closeModal();
    const itemsCount = updateCartTotal();

    if (!itemsCount) {
      document.querySelector('.cart').closest('form').remove();
      document
        .querySelector('main')
        .append((document.createElement('h2').textContent = 'Rien dans le panier...'));
    }
  } catch (e) {
    console.error(e);
  }
}

function closeModal() {
  document.getElementById('backdrop').remove();
}

function createValidationPopup(params) {
  const { name, btn, id } = params;
  const template = document.getElementById('delete-item-template');
  const backdropElement = template.cloneNode(true).content;
  const formElement = backdropElement.querySelector('form');
  const textElement = formElement.querySelector('p');
  textElement.textContent = `Voulez vous vraiment supprimer "${name}"?`;

  const cancelButton = backdropElement.getElementById('cancel-btn');

  cancelButton.addEventListener('click', closeModal);

  formElement.addEventListener('submit', deleteProductFromCart.bind(null, btn, id));

  document.body.append(backdropElement);
}

// on récupère les champs qui contiennent les quantités panier
const quantityInputElements = Array.from(document.querySelectorAll('input[type="number"'));

// on boucle pour recuperer les champs un par un
for (const input of quantityInputElements) {
  // on recupère le prix du produit qui est "stocké" dans un input caché qui est l'élément précedent de notre champs
  const itemPrice = +input.previousElementSibling.value;
  const productElement = input.closest('.product');
  const productName = productElement.querySelector('.product__header span').textContent;
  // on récupère l'élément "frère" du parent de notre champs
  const inputParentElement = input.parentElement;
  const inputPriceElement = inputParentElement.parentElement.nextElementSibling;
  // on récupère le nombre d'éléments de base
  const baseQuantity = +input.value;

  const user_id = +input.previousElementSibling.previousElementSibling.value;
  const product_id = +input.previousElementSibling.previousElementSibling.previousElementSibling
    .value;

  const trashElement = input.nextElementSibling;

  updateCartTotal();

  trashElement.addEventListener('click', async (event) => {
    // fais apparaitre une popup de validation
    const trashButtonElement = event.target;
    createValidationPopup({
      btn: trashButtonElement,
      name: productName,
      id: product_id,
    });
  });

  input.addEventListener('input', (event) => {
    const itemQuantity = +event.target.value;
    // on change le prix du coté navigateur
    inputPriceElement.textContent = `${itemQuantity * itemPrice}€`;

    updateCartTotal();
  });

  // se déclenche quand l'élément perd le focus
  input.addEventListener('blur', async (event) => {
    const quantity = +event.target.value;

    try {
      // on envoie une requete au serveur pour faire persister les changements en base de donnée
      await axios.post(
        `https://${
          window.location.hostname === 'localhost'
            ? `${window.location.hostname}:8080`
            : window.location.hostname
        }/api/panier`,
        {
          quantity: quantity - baseQuantity,
          product_id,
          user_id,
        },
      );

      if (!quantity) {
        input.closest('.product').remove();
      }

      triggerNotification('Panier mis à jour !');
    } catch (error) {
      console.error(error);
    }
  });
}
