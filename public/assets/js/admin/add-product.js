function previewFile() {
  const preview = document.querySelector('#product-image');
  const file = document.querySelector('#file').files[0];
  const reader = new FileReader();

  reader.addEventListener('loadend', () => (preview.src = reader.result));

  if (file) {
    reader.readAsDataURL(file);
  } else {
    preview.src = '';
  }
}

const fileInputElement = document.querySelector('#file');

fileInputElement.addEventListener('change', previewFile);
