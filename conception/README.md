# DW SHOP

## Besoins du site

- espace connexion
- espace utilisateur
- panier/wishlist
- barre de recherche
- logo/nom
- images produits
- système d'avis produits/notation
- base de donnée
- moyen de paiement
- section administrateur
- page contact/moyen de contact
- partage reseau social

---

## User stories

| en tant que | je veux                             | afin de                                |
| ----------- | ----------------------------------- | -------------------------------------- |
| visiteur    | acceder à la page produits          | visualiser les produits                |
| visiteur    | acceder à la barre de recherche     | visualiser des produits en particulier |
| visiteur    | pouvoir mettre un article en panier | acheter les produits                   |
| visiteur    | agir sur le panier                  | supprimer un produit/quantité          |
| visiteur    | acceder à la page inscription       | m'inscrire                             |
| visiteur    | acceder à la page connexion         | me connecter                           |
| client      | agir sur le panier                  | acheter les articles                   |
| client      | choisir le mode de paiement         | choisir celui qui convient             |
| client      | choisir le mode de livraison        | choisir celle qui convient             |
| client      | avoir un historique de commande     | retrouver mes commandes                |
| admin       | avoir une interface admin           | gerer mes produits                     |
| admin       | avoir une interface admin           | gerer mes categories                   |
| admin       | avoir une interface admin           | supprimer des comptes utilisateur      |
| admin       | avoir une interface admin           | gerer les moyens de paiement           |

---

## BDD

Entités:

- users
- products
- product_details
- cart
- orders
- categories

- wishlist (?)
- reviews (?)
- payments (?)
- payment_method (?)

MCD

owns, 11 users, 11 cart
cart: cart_id, created_at, updated_at
contains, 0N cart, 0N products
product_details: product_detail_id, color, size, quantity, image_url, created_at, updated_at

users: user_id, first_name, last_name, age, email, password, address, postal_code, city, country, is_admin, created_at, updated_at
order_details, 11 orders, 11 products
products: product_id, name, description, price, created_at, updated_at
defines, 1N products, 11 product_details

can, 0N users, 11 orders
orders: order_id, status, shipping_date, prepare_date, shipping_estimate, created_at, updated_at
categorize, 1N products, 0N categories
categories: category_id, name, color, created_at, updated_at
