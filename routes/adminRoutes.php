<?php

$router->map('GET', '/admin', 'redirectAdmin');
$router->map('GET', '/admin/produits', 'getAdminProducts');
$router->map('GET', '/admin/produits/ajouter', 'getAddAdminProducts');
$router->map('POST', '/admin/produits/ajouter', 'postAddAdminProducts');
$router->map('POST', '/admin/produits/supprimer/[i:product_id]', 'deleteAdminProduct');
$router->map('GET', '/admin/produits/modifier/[i:product_id]', 'getEditAdminProducts');
$router->map('POST', '/admin/produits/modifier/[i:product_id]', 'postAddAdminProducts');

$router->map('GET', '/admin/categories', 'getAdminCategories');
$router->map('GET', '/admin/categories/ajouter', 'getAddAdminCategory');
$router->map('POST', '/admin/categories/ajouter', 'postAddAdminCategory');
$router->map('POST', '/admin/categories/supprimer/[i:category_id]', 'deleteAdminCategory');
$router->map('GET', '/admin/categories/modifier/[i:category_id]', 'getEditAdminCategory');
$router->map('POST', '/admin/categories/modifier/[i:category_id]', 'postAddAdminCategory');

$router->map('GET', '/admin/utilisateurs', 'getAdminUsers');
$router->map('POST', '/admin/utilisateurs/supprimer/[i:user_id]', 'deleteAdminUser');

$router->map('GET', '/admin/commandes', 'getAdminOrders');