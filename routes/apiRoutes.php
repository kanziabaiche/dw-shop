<?php

$router->map('POST', '/api/panier', 'postAddToCart');

$router->map('DELETE', "/api/panier/[i:product_id]", 'postDeleteCartItemByProductId');