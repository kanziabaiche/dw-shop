<?php

$router->map('GET', '/', 'getHome');
$router->map('GET', '/produits', 'getProductsPage');
$router->map('GET', '/produits/[i:product_id]', 'getProductPage');
$router->map('GET', '/contact', 'getContact');

$router->map('GET', '/panier', 'getCartPage');
$router->map('GET', '/commande/valider', 'getValidateOrderPage');
$router->map('POST', '/commande/paiement', 'postValidateOrderPage');
$router->map('POST', '/commande/paiement/success', 'postPayment');

$router->map('GET', '/login', 'getLogin');
$router->map('POST', '/login', 'postLogin');
$router->map('GET', '/inscription', 'getSignup');
$router->map('POST', '/inscription', 'postSignup');
$router->map('GET', '/logout', 'postLogout');