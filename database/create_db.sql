DROP DATABASE IF EXISTS `dw_shop`;

CREATE DATABASE `dw_shop` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `dw_shop`;

CREATE TABLE `users` (
  `user_id` BIGINT AUTO_INCREMENT PRIMARY KEY,
  `first_name` VARCHAR(42) NOT NULL,
  `last_name` VARCHAR(42) NOT NULL,
  `age` INT NOT NULL,
  `email` VARCHAR(42) UNIQUE NOT NULL,
  `password` VARCHAR(255),
  `address` VARCHAR(255),
  `postal_code` INT NOT NULL,
  `city` VARCHAR(42) NOT NULL,
  `country` VARCHAR(42) NOT NULL,
  `is_admin` BOOLEAN DEFAULT false,
  `created_at` TIMESTAMP DEFAULT NOW(),
  `updated_at` TIMESTAMP DEFAULT NOW()
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `products` (
  `product_id` BIGINT AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(42),
  `description` TEXT,
  `price` INT NOT NULL,
  `quantity` INT NOT NULL,
  `weight` VARCHAR(15) NOT NULL,
  `image_url` TEXT,
  `created_at` TIMESTAMP DEFAULT NOW(),
  `updated_at` TIMESTAMP DEFAULT NOW()
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `cart` (
  `user_id` BIGINT NOT NULL REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `product_id` BIGINT NOT NULL REFERENCES `products`(`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `quantity` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT NOW(),
  `updated_at` TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY (`user_id`, `product_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `orders` (
  `order_id` BIGINT AUTO_INCREMENT PRIMARY KEY,
  `status` VARCHAR(42),
  `shipping_date` TIMESTAMP,
  `prepare_date` TIMESTAMP,
  `shipping_estimate` DATE,
  `user_id` BIGINT NOT NULL REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `created_at` TIMESTAMP DEFAULT NOW(),
  `updated_at` TIMESTAMP DEFAULT NOW()
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `order_items` (
  `order_item_id` BIGINT AUTO_INCREMENT PRIMARY KEY,
  `user_id` BIGINT NOT NULL REFERENCES `users`(`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `product_id` BIGINT NOT NULL REFERENCES `products`(`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `order_id` BIGINT NOT NULL REFERENCES `orders`(`order_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `quantity` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT NOW(),
  `updated_at` TIMESTAMP DEFAULT NOW()
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

CREATE TABLE `categories` (
  `category_id` BIGINT AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(30) UNIQUE,
  `color` VARCHAR(7) UNIQUE DEFAULT "#ffffff",
  `created_at` TIMESTAMP DEFAULT NOW(),
  `updated_at` TIMESTAMP DEFAULT NOW()
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- CREATE TABLE `product_details` (
--   `product_detail_id` BIGINT AUTO_INCREMENT PRIMARY KEY,
--   `color` VARCHAR(42),
--   `size` VARCHAR(6),
--   `quantity` INT DEFAULT 0,
--   `image_url` TEXT,
--   `product_id` BIGINT NOT NULL REFERENCES `products` (`product_id`),
--   `created_at` TIMESTAMP DEFAULT NOW(),
--   `updated_at` TIMESTAMP DEFAULT NOW()
-- ) ENGINE = InnoDB DEFAULT CHARSET = utf8;
CREATE TABLE `category_categorize_product` (
  `product_id` BIGINT NOT NULL REFERENCES `products`(`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  `category_id` BIGINT NOT NULL REFERENCES `categories`(`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`product_id`, `category_id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

INSERT INTO users(
    first_name,
    last_name,
    age,
    email,
    PASSWORD,
    address,
    postal_code,
    city,
    country,
    is_admin
  )
VALUES (
    'John',
    'Doe',
    45,
    'johndoe@test.com',
    '$2y$14$fKBLLkng0oSLiQqzhvFEH.DQ0MONyb2Dv2kg.s7uT0j.8mVA2VML6',
    '3 rue du test',
    '12345',
    'testville',
    'France',
    TRUE
  );